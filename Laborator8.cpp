#include "Laborator8.h"

#include <vector>
#include <string>
#include <iostream>

#include <Core/Engine.h>

using namespace std;

Laborator8::Laborator8()
{
}

Laborator8::~Laborator8()
{
}

void Laborator8::Init()
{
	const string textureLoc = "Source/Laboratoare/Laborator8/Textures";

	// Load textures
	{
		Texture2D* texture = new Texture2D();
		texture->Load2D((textureLoc + "/santa_diffuse.png").c_str(), GL_REPEAT);
		mapTextures["santa_diffuse"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D((textureLoc + "/santa_specular.png").c_str(), GL_REPEAT);
		mapTextures["santa_specular"] = texture;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D((textureLoc + "/grass.png").c_str(), GL_REPEAT);
		//mapTextures["grass"] = texture;
	}


	// Load meshes
	{
		Mesh* mesh = new Mesh("santa");
		mesh->LoadMesh(textureLoc, "santa.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("plane");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "plane50.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("sphere");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "sphere.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	// Create a shader program for drawing face polygon with the color of the normal
	{
		Shader *shader = new Shader("ShaderLab8");
		shader->AddShader("Source/Laboratoare/Laborator8/Shaders/VertexShader.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Laboratoare/Laborator8/Shaders/FragmentShader.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;
	}

	//Light & material properties
	{
		lightPosition1 = glm::vec3(0, 2, 1.1);
		lightPosition2 = glm::vec3(0, 2, 1.2);
		lightPosition3 = glm::vec3(0, 2, 1.3);
		lightPosition4 = glm::vec3(0, 2, 1.4);
		lightDirection = glm::vec3(0, -1, 0);
		materialShininess = 30;
		materialKd = 0.5;
		materialKs = 0.5;
	}
}

void Laborator8::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);	
}

void Laborator8::Update(float deltaTimeSeconds)
{

	if (camera_type == 1) {
		GetSceneCamera()->SetPosition(glm::vec3(0, 2, 0));
	}

	if (pause == false) {
		val++;
		lightPosition1 = glm::vec3(1.5 * sin(RADIANS(val)), 1 + sin(RADIANS(val)), 1.5 * cos(RADIANS(val)));
		lightPosition2 = lightPosition1 + glm::vec3(0.5 * sin(RADIANS(val * 3)), 0.3 * sin(RADIANS(val * 3)), 0.5 * cos(RADIANS(val * 3)));
		lightPosition3 = glm::vec3(1.5 * cos(RADIANS(val)), 1, 1.5 * sin(RADIANS(val)));
		lightPosition4 = glm::vec3(1.5 * cos(RADIANS(val)), 1, 1.5 * cos(RADIANS(val)));
	}

	if (camera_type != 1) {
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 0.2, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.01f));
		RenderSimpleMesh(meshes["santa"], shaders["ShaderLab8"], modelMatrix, glm::vec3(1, 1, 1), mapTextures["santa_diffuse"], mapTextures["santa_specular"], 0);
	}

	// Santa with Toon Shading
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	if (camera_type != 1) {
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 0.2, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.01f));
		RenderSimpleMesh(meshes["santa"], shaders["ShaderLab8"], modelMatrix, glm::vec3(1, 1, 1), mapTextures["santa_diffuse"], mapTextures["santa_specular"], 1);
	}
	glDisable(GL_CULL_FACE);


	// Render ground
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0, 0.01f, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.25f));
		RenderSimpleMesh(meshes["plane"], shaders["ShaderLab8"], modelMatrix, glm::vec3(1, 1, 1), mapTextures["santa_diffuse"], mapTextures["santa_specular"], 0);
	}

	// Render the point light in the scene
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, lightPosition1);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1));
		RenderSimpleMesh(meshes["sphere"], shaders["Simple"], modelMatrix, glm::vec3(0.3), mapTextures["santa_diffuse"], mapTextures["santa_specular"], 0);
	}

	// Render the point light in the scene
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, lightPosition2);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1));
		RenderSimpleMesh(meshes["sphere"], shaders["Simple"], modelMatrix, glm::vec3(1, 0, 0), mapTextures["santa_diffuse"], mapTextures["santa_specular"], 0);
	}

	// Render the point light in the scene
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, lightPosition3);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1));
		RenderSimpleMesh(meshes["sphere"], shaders["Simple"], modelMatrix, glm::vec3(0, 1, 0), mapTextures["santa_diffuse"], mapTextures["santa_specular"], 0);
	}

	// Render the point light in the scene
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, lightPosition4);
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.1));
		RenderSimpleMesh(meshes["sphere"], shaders["Simple"], modelMatrix, glm::vec3(0, 0, 1), mapTextures["null"], mapTextures["null"], 0);
	}
}

void Laborator8::FrameEnd()
{
	DrawCoordinatSystem();
}

void Laborator8::RenderSimpleMesh(Mesh *mesh, Shader *shader, const glm::mat4 & modelMatrix, const glm::vec3 &color, Texture2D* texture1, Texture2D* texture2, int stroke)
{
	if (!mesh || !shader || !shader->GetProgramID())
		return;

	// render an object using the specified shader and the specified position
	glUseProgram(shader->program);

	// Set shader uniforms for light & material properties
	// TODO: Set light position uniform
	int light_position1 = glGetUniformLocation(shader->program, "light_position1");
	glUniform3f(light_position1, lightPosition1.x, lightPosition1.y, lightPosition1.z);

	int light_position2 = glGetUniformLocation(shader->program, "light_position2");
	glUniform3f(light_position2, lightPosition2.x, lightPosition2.y, lightPosition2.z);

	int light_position3 = glGetUniformLocation(shader->program, "light_position3");
	glUniform3f(light_position3, lightPosition3.x, lightPosition3.y, lightPosition3.z);

	int light_position4 = glGetUniformLocation(shader->program, "light_position4");
	glUniform3f(light_position4, lightPosition4.x, lightPosition4.y, lightPosition4.z);

	int light_direction = glGetUniformLocation(shader->program, "light_direction");
	glUniform3f(light_direction, lightDirection.x, lightDirection.y, lightDirection.z);

	// TODO: Set eye position (camera position) uniform
	glm::vec3 eyePosition = GetSceneCamera()->transform->GetWorldPosition();
	int eye_position = glGetUniformLocation(shader->program, "eye_position");
	glUniform3f(eye_position, eyePosition.x, eyePosition.y, eyePosition.z);

	// TODO: Set material property uniforms (shininess, kd, ks, object color) 
	int material_shininess = glGetUniformLocation(shader->program, "material_shininess");
	glUniform1i(material_shininess, materialShininess);

	int loc_level = glGetUniformLocation(shader->program, "num_levels");
	glUniform1i(loc_level, 3);

	int material_kd = glGetUniformLocation(shader->program, "material_kd");
	glUniform1f(material_kd, materialKd);

	int material_ks = glGetUniformLocation(shader->program, "material_ks");
	glUniform1f(material_ks, materialKs);

	int object_color = glGetUniformLocation(shader->program, "object_color");
	glUniform3f(object_color, color.r, color.g, color.b);

	// Bind model matrix
	GLint loc_model_matrix = glGetUniformLocation(shader->program, "Model");
	glUniformMatrix4fv(loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	// Bind view matrix
	glm::mat4 viewMatrix = GetSceneCamera()->GetViewMatrix();
	int loc_view_matrix = glGetUniformLocation(shader->program, "View");
	glUniformMatrix4fv(loc_view_matrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));

	int loc_switch = glGetUniformLocation(shader->program, "switc");
	glUniform1f(loc_switch, switc);


	// Bind projection matrix
	glm::mat4 projectionMatrix = GetSceneCamera()->GetProjectionMatrix();
	int loc_projection_matrix = glGetUniformLocation(shader->program, "Projection");
	glUniformMatrix4fv(loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	int loc_time = glGetUniformLocation(shader->program, "Time");
	glUniform1f(loc_time, (float)Engine::GetElapsedTime());

	int loc_stroke = glGetUniformLocation(shader->program, "Stroke");
	glUniform1i(loc_stroke, stroke);


	if (texture1)
	{
		//TODO : activate texture location 0
		glActiveTexture(GL_TEXTURE0);

		//TODO : Bind the texture1 ID
		glBindTexture(GL_TEXTURE_2D, texture1->GetTextureID());

		//TODO : Send texture uniform value
		glUniform1i(glGetUniformLocation(shader->program, "texture_1"), 0);
	}

	if (texture2)
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2->GetTextureID());
		glUniform1i(glGetUniformLocation(shader->program, "texture_2"), 1);
		//TODO : activate texture location 1
		//TODO : Bind the texture2 ID
		//TODO : Send texture uniform value
	}

	// Draw the object
	glBindVertexArray(mesh->GetBuffers()->VAO);
	glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}

// Documentation for the input functions can be found in: "/Source/Core/Window/InputController.h" or
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/Window/InputController.h

void Laborator8::OnInputUpdate(float deltaTime, int mods)
{
	float speed = 2;

	if (!window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		glm::vec3 up = glm::vec3(0, 1, 0);
		glm::vec3 right = GetSceneCamera()->transform->GetLocalOXVector();
		glm::vec3 forward = GetSceneCamera()->transform->GetLocalOZVector();
		forward = glm::normalize(glm::vec3(forward.x, 0, forward.z));

		// Control light position using on W, A, S, D, E, Q
		if (window->KeyHold(GLFW_KEY_W)) lightPosition1 -= forward * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_A)) lightPosition1 -= right * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_S)) lightPosition1 += forward * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_D)) lightPosition1 += right * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_E)) lightPosition1 += up * deltaTime * speed;
		if (window->KeyHold(GLFW_KEY_Q)) lightPosition1 -= up * deltaTime * speed;
	}

	if (window->KeyHold(GLFW_KEY_7)) unghi ++;
	if (window->KeyHold(GLFW_KEY_8)) unghi --;
}

void Laborator8::OnKeyPress(int key, int mods)
{
	if (key == GLFW_KEY_1) {
		switc = (switc + 1) % 2;
	}

	if (key == GLFW_KEY_UP) {
		lightDirection = glm::vec3(0, 1, 0);
	}

	if (key == GLFW_KEY_RIGHT) {
		lightDirection = glm::vec3(1, 0, 0);
	}

	if (key == GLFW_KEY_DOWN) {
		lightDirection = glm::vec3(0, -1, 0);
	}

	if (key == GLFW_KEY_LEFT) {
		lightDirection = glm::vec3(-1, 0, 0);
	}

	if (key == GLFW_KEY_F) {
		GetSceneCamera()->SetPosition(glm::vec3(0, 2, 0));
		GetSceneCamera()->SetRotation(glm::quat(0, 0, 1, 0));
		camera_type = 1;
	}

	if (key == GLFW_KEY_T) {
		GetSceneCamera()->SetPosition(glm::vec3(0, 2, 2));
		GetSceneCamera()->SetRotation(glm::quat(0, 0, 0, 0));
		camera_type = 3;
	}

	if (key == GLFW_KEY_P) {
		pause = ! pause;
	}


	// add key press event
}

void Laborator8::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator8::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
}

void Laborator8::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator8::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator8::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator8::OnWindowResize(int width, int height)
{
}
