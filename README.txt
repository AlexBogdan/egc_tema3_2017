-=-=-=-=-=-=- Andrei Bogdan Alexandru =-=-=-=-=-=-=-=
=-=-=-=-=-=-=-==-=-=-=-= 336 CB -=-=-=-=-=-=-=-=-=-=
--=-=-=-=-=-=-=-=-=-=-=- Tema 3 EGC =-=-=-=-=-=-=-=-==-

Pentru implementarea temei am folosit laboratoarele 7, 8, 9.

	Modelul si textura lui Mos Craciun au fost cele oferite in
enuntul temei. Texturarea a fost realizata folosind functia si
codul de Shader din laboratorul 9.

	Toon Shadind (Stroke)
	Pentru a realiza efectul de Toon Shading am rendat acealsi model
folosind culoarea neagra, marind is micsorand aceasta imagine.

	Stepped Lighting
	Pentru acest efect am modificat formulele din laboratorul 8 pentru 
a avea noua formula oferita in enunt

	Luminile
	Culoarea luminilor este asociata direct in FragmentShader, in
momentul in care calculam fiecare lumina in parte.

	Camera
	FirstPerson = camera este dusa in locul lui Mos Craciun si este permisa
doar rotatia camerei
	ThirdPerson = camera este lasata libera in scena