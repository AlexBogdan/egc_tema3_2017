#version 330

// TODO: get color value from vertex shader
in vec3 world_position;
in vec3 world_normal;

in vec2 texcoord;

// Uniforms for light properties
uniform vec3 light_position1;
uniform vec3 light_position2;
uniform vec3 light_position3;
uniform vec3 light_position4;

uniform vec3 light_direction;

uniform vec3 eye_position;
uniform float switc;
uniform int unghi;
uniform sampler2D texture_1;
uniform sampler2D texture_2;

uniform int num_levels;

uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;

uniform vec3 object_color;

uniform float Time;
uniform int Stroke;

layout(location = 0) out vec4 out_color;

void main()
{
	float ka = 0.25;
	int culoare_lumina = 1;
	vec3 color11, color12, color13, color14;

	// Extragem kd si ks din cele 2 texturi (din cerinta)
	vec4 color1 = texture2D(texture_1, texcoord);
	vec3 kd = color1.xyz;

	vec4 color2 = texture2D(texture_2, texcoord);
	vec3 ks = color2.xyz;

	// TODO: compute light1
	{
		int primeste_lumina = 0;
		float specular_light = 0;
		float diffuse_light = 0;

		vec3 N = normalize( world_normal);
		vec3 L = normalize( light_position1 - world_position );
		vec3 V = normalize( eye_position - world_position );
		vec3 H = normalize( L + V );

		// compute diffuse light component
		diffuse_light = floor(clamp(dot(N,L), 0, 1) * num_levels) / num_levels;

		// compute specular light component
		if (dot(N,  L) > 0) {
			primeste_lumina = 1;
		}
		if (diffuse_light > 0) {
			specular_light = culoare_lumina *  primeste_lumina * pow( max(dot(N, H), 0), material_shininess);
			specular_light = floor(specular_light * num_levels) / num_levels;
		}
	
		// compute attenuation factor
		float d = distance(light_position1, world_position);
		float factor_atenuare = 1 / (d*d);

		// compute color
		color11 = ((diffuse_light + ka) * kd + specular_light * ks) * factor_atenuare * vec3(1, 0.55, 0);
	}

	// TODO: compute light2
	{
		int primeste_lumina = 0;
		float specular_light = 0;
		float diffuse_light = 0;

		vec3 N = normalize( world_normal);
		vec3 L = normalize( light_position2 - world_position );
		vec3 V = normalize( eye_position - world_position );
		vec3 H = normalize( L + V );

		// compute diffuse light component
		diffuse_light = floor(clamp(dot(N,L), 0, 1) * num_levels) / num_levels;

		// compute specular light component
		if (dot(N,  L) > 0) {
			primeste_lumina = 1;
		}
		if (diffuse_light > 0) {
			specular_light = culoare_lumina *  primeste_lumina * pow( max(dot(N, H), 0), material_shininess);
			specular_light = floor(specular_light * num_levels) / num_levels;
		}
	
		// compute attenuation factor
		float d = distance(light_position2, world_position);
		float factor_atenuare = 1 / (d*d);

		// compute color magenta
		color12 = ((diffuse_light + ka) * kd + specular_light * ks) * factor_atenuare * vec3(1, 0, 1);
	}

	// TODO: compute light3
	{
		int primeste_lumina = 0;
		float specular_light = 0;
		float diffuse_light = 0;

		vec3 N = normalize( world_normal);
		vec3 L = normalize( light_position3 - world_position );
		vec3 V = normalize( eye_position - world_position );
		vec3 H = normalize( L + V );

		// compute diffuse light component
		diffuse_light = floor(clamp(dot(N,L), 0, 1) * num_levels) / num_levels;

		// compute specular light component
		if (dot(N,  L) > 0) {
			primeste_lumina = 1;
		}
		if (diffuse_light > 0) {
			specular_light = culoare_lumina *  primeste_lumina * pow( max(dot(N, H), 0), material_shininess);
			specular_light = floor(specular_light * num_levels) / num_levels;
		}
	
		// compute attenuation factor
		float d = distance(light_position3, world_position);
		float factor_atenuare = 1 / (d*d);

		// compute color green
		color13 = ((diffuse_light + ka) * kd + specular_light * ks) * factor_atenuare * vec3(0, 1, 0);
	}

	// TODO: compute light4
	{
		int primeste_lumina = 0;
		float specular_light = 0;
		float diffuse_light = 0;

		vec3 N = normalize( world_normal);
		vec3 L = normalize( light_position4 - world_position );
		vec3 V = normalize( eye_position - world_position );
		vec3 H = normalize( L + V );

		// compute diffuse light component
		diffuse_light = floor(clamp(dot(N,L), 0, 1) * num_levels) / num_levels;

		// compute specular light component
		if (dot(N,  L) > 0) {
			primeste_lumina = 1;
		}
		if (diffuse_light > 0) {
			specular_light = culoare_lumina *  primeste_lumina * pow( max(dot(N, H), 0), material_shininess);
			specular_light = floor(specular_light * num_levels) / num_levels;
		}
	
		// compute attenuation factor
		float d = distance(light_position4, world_position);
		float factor_atenuare = 1 / (d*d);

		// compute color blue
		color14 = ((diffuse_light + ka) * kd + specular_light * ks) * factor_atenuare * vec3(0, 0 , 1);
	}

	if (Stroke == 0) {
		out_color = vec4(color11 + color12 + color13 + color14,    1);
	} else {
		out_color = vec4(0);
	}

}