#version 330

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_texture_coord;
layout(location = 3) in vec3 v_color;

// Uniform properties
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

uniform float Time;
uniform int Stroke;

// Output values to fragment shader
out vec3 world_position;
out vec3 world_normal;

out vec2 texcoord;

void main()
{
	// TODO: compute world space vertex position and normal
	
	vec3 v_pos;

	if (Stroke == 1) {
		v_pos = v_position + sin(Time * 1.5) * v_normal;
	}
	else {
		v_pos = v_position;
	}

	world_position = (Model * vec4(v_pos,1)).xyz;
	world_normal = normalize( mat3(Model) * v_normal );
	

	// TODO : pass v_texture_coord as output to Fragment Shader
	texcoord = v_texture_coord;

	gl_Position = Projection * View * Model * vec4(v_pos, 1.0);
}
